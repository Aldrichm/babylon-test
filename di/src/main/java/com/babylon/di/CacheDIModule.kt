package com.babylon.di

import com.babylon.cache.di.cacheDIModule

/**
 * Koin Module to provide dependenices for all Database Related Classes
 */
val cacheDIModule = cacheDIModule
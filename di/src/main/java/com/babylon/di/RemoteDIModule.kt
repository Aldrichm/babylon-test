package com.babylon.di

import com.babylon.remote.di.remoteDIModule

/**
 * Koin Module to provide dependenices for all Network Related Classes
 */
val remoteDIModule = remoteDIModule
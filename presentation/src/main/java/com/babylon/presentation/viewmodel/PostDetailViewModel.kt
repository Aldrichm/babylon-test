package com.babylon.presentation.viewmodel

import androidx.lifecycle.*
import com.babylon.presentation.model.CommentView
import com.babylon.presentation.model.PostView
import com.babylon.presentation.model.UserView
import com.babylon.presentation.repository.CommentDataRepository
import com.babylon.presentation.repository.PostDataRepository
import com.babylon.presentation.repository.UserDataRepository
import com.babylon.presentation.resource.Resource

class PostDetailViewModel(
    private val postDataRepository: PostDataRepository,
    private val userDataRepository: UserDataRepository,
    private val commentDataRepository: CommentDataRepository
) : ViewModel(), LifecycleObserver {


    private val _post: MediatorLiveData<Resource<PostView>> = MediatorLiveData()
    val post: LiveData<Resource<PostView>>
        get() = _post

    private val _commentList: MediatorLiveData<Resource<List<CommentView>>> = MediatorLiveData()
    val commentList: LiveData<Resource<List<CommentView>>>
        get() = _commentList


    private val _user: MediatorLiveData<Resource<UserView>> = MediatorLiveData()
    val user: LiveData<Resource<UserView>>
        get() = _user

    //Listen to data set from the Activity
    internal val _userId: MediatorLiveData<String> = MediatorLiveData()
    internal val _postId: MediatorLiveData<String> = MediatorLiveData()

    init {
        _post.addSource(_postId) {
            _post.removeSource(_postId)
            _post.addSource(postDataRepository.getPostWithId(it)) {
                _post.value = it
            }
        }

        _user.addSource(_userId) {
            _user.removeSource(_userId)
            _user.addSource(userDataRepository.getUserWithId(it)) {
                _user.value = it
            }
        }

        _commentList.addSource(_postId) {
            _commentList.removeSource(_postId)
            _commentList.addSource(commentDataRepository.getCommentsForPostId(it)) {
                _commentList.value = it
            }
        }

    }

    //This toString is really bad and should accept Int directly instead.
    fun setData(userId: Int, postId: Int) {
        _userId.value = userId.toString()
        _postId.value = postId.toString()

    }

}

package com.babylon.presentation.viewmodel

import androidx.lifecycle.*
import com.babylon.presentation.model.PostView
import com.babylon.presentation.repository.PostDataRepository
import com.babylon.presentation.resource.Resource

class PostListViewModel(
    private val postDataRepository: PostDataRepository
) : ViewModel(), LifecycleObserver {

    private val _postList: MediatorLiveData<Resource<List<PostView>>> = MediatorLiveData()
    val postList: LiveData<Resource<List<PostView>>>
        get() = _postList

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun getData(){
        _postList.addSource(postDataRepository.getPosts()){
            _postList.value = it
        }
    }

}
package com.babylon.presentation.model

data class CommentView(
    val postId: Int,
    val id: Int,
    val name: String,
    val email: String,
    val body: String
)

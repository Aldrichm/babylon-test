package com.babylon.presentation.model

data class UserView(
    val id: Int,
    val email: String,
    val name: String,
    val phone: String,
    val username: String,
    val website: String
)
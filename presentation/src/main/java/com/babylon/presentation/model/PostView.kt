package com.babylon.presentation.model

data class PostView(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String
)

package com.babylon.presentation.repository

import androidx.lifecycle.LiveData
import com.babylon.presentation.model.CommentView
import com.babylon.presentation.resource.Resource

interface CommentDataRepository {

    fun getCommentsForPostId(postId : String) : LiveData<Resource<List<CommentView>>>

}
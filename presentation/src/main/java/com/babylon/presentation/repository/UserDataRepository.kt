package com.babylon.presentation.repository

import androidx.lifecycle.LiveData
import com.babylon.presentation.model.UserView
import com.babylon.presentation.resource.Resource

interface UserDataRepository {

    fun getUserWithId(id: String): LiveData<Resource<UserView>>

}
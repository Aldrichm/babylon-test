package com.babylon.presentation.repository

import androidx.lifecycle.LiveData
import com.babylon.presentation.model.PostView
import com.babylon.presentation.resource.Resource

interface PostDataRepository {

    fun getPosts(): LiveData<Resource<List<PostView>>>

    fun getPostWithId(id : String): LiveData<Resource<PostView>>
}
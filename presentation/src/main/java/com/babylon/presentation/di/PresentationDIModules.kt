package com.babylon.presentation.di

import com.babylon.presentation.viewmodel.PostDetailViewModel
import com.babylon.presentation.viewmodel.PostListViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val presentationDIModule = module {

    viewModel {
        PostListViewModel(
            postDataRepository = get()
        )
    }

    viewModel {
        PostDetailViewModel(
            userDataRepository = get(),
            commentDataRepository = get(),
            postDataRepository = get()
        )
    }
}
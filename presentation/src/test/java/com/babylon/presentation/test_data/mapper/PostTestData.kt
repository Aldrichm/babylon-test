import com.babylon.presentation.model.PostView

object PostTestData {

    val postView1 = PostView(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val postView2 = PostView(
        userId = 2,
        id = 2,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val postView3 = PostView(
        userId = 3,
        id = 3,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val postViewList = listOf(postView1, postView2, postView3)
}
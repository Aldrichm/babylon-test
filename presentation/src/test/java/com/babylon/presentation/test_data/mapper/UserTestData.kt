import com.babylon.presentation.model.UserView

object UserTestData {

    val userView = UserView(
        id = 1,
        email = "testy@test.com",
        name = "Testy Mc Test face",
        phone = "0000-000-0000",
        username = "Testy",
        website = "testy@email.com"
    )

}
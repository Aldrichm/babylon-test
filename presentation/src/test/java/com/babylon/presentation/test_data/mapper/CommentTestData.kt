import com.babylon.presentation.model.CommentView

object CommentTestData {

    val commentView1 = CommentView(
        postId = 1,
        id = 1,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val commentView2 = CommentView(
        postId = 2,
        id = 2,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val commentView3 = CommentView(
        postId = 3,
        id = 3,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val commentViewList = listOf(commentView1, commentView2, commentView3)

}
package com.babylon.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.babylon.presentation.mockPresentationDIModule
import com.babylon.presentation.model.CommentView
import com.babylon.presentation.model.PostView
import com.babylon.presentation.model.UserView
import com.babylon.presentation.repository.CommentDataRepository
import com.babylon.presentation.repository.PostDataRepository
import com.babylon.presentation.repository.UserDataRepository
import com.babylon.presentation.resource.Resource
import org.junit.*
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.koin.test.declare
import org.koin.test.declareMock
import org.koin.test.mockInstance
import org.mockito.Mock
import org.mockito.Mockito

class PostDetailViewModelTest : KoinTest {

    // Executes tasks in the Architecture Components in the same thread
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val postDetailViewModel: PostDetailViewModel by inject()

    private val postDataRepository = Mockito.mock(PostDataRepository::class.java)
    private val userDataRepository = Mockito.mock(UserDataRepository::class.java)
    private val commentDataRepository = Mockito.mock(CommentDataRepository::class.java)

    @Before
    fun setUp() {
        startKoin(listOf(
            module {
                viewModel {
                    PostDetailViewModel(
                        postDataRepository = postDataRepository,
                        userDataRepository = userDataRepository,
                        commentDataRepository = commentDataRepository
                    )
                }
            }
        ))
    }

    @After
    fun tearDown() {
        stopKoin()
    }

    @Test
    fun getPost() {

        //Add a mock observer to our LiveData so that we can verify the data is changed when we expect it to.
        val observer = Mockito.mock(Observer::class.java) as Observer<Resource<PostView>>
        postDetailViewModel.post.observeForever(observer)

        //Mock PostDataRepository
        val result = MutableLiveData<Resource<PostView>>()
        val postView = PostTestData.postView1
        result.value = Resource.success(postView)
        Mockito.`when`(
            postDataRepository.getPostWithId(Mockito.anyString())
        ).thenReturn(
            result
        )

        //Trigger source
        postDetailViewModel._postId.value = Mockito.anyString()

        Mockito.verify(observer).onChanged(Resource.success(postView))
        Assert.assertEquals(Resource.success(postView), postDetailViewModel.post.value)

    }

    @Test
    fun getCommentList() {

        //Add a mock observer to our LiveData so that we can verify the data is changed when we expect it to.
        val observer = Mockito.mock(Observer::class.java) as Observer<Resource<List<CommentView>>>
        postDetailViewModel.commentList.observeForever(observer)

        //Mock CommentDataRepository
        val result = MutableLiveData<Resource<List<CommentView>>>()
        val commentViewList = CommentTestData.commentViewList
        result.value = Resource.success(commentViewList)
        Mockito.`when`(
            commentDataRepository.getCommentsForPostId(Mockito.anyString())
        ).thenReturn(
            result
        )

        //Trigger source
        postDetailViewModel._postId.value = Mockito.anyString()

        Mockito.verify(observer).onChanged(Resource.success(commentViewList))
        Assert.assertEquals(Resource.success(commentViewList), postDetailViewModel.commentList.value)

    }

    @Test
    fun getUser() {

        //Add a mock observer to our LiveData so that we can verify the data is changed when we expect it to.
        val observer = Mockito.mock(Observer::class.java) as Observer<Resource<UserView>>
        postDetailViewModel.user.observeForever(observer)

        //Mock UserDataRepository
        val result = MutableLiveData<Resource<UserView>>()
        val userView = UserTestData.userView
        result.value = Resource.success(userView)
        Mockito.`when`(
            userDataRepository.getUserWithId(Mockito.anyString())
        ).thenReturn(
            result
        )

        //Trigger source
        postDetailViewModel._userId.value = Mockito.anyString()

        Mockito.verify(observer).onChanged(Resource.success(userView))
        Assert.assertEquals(Resource.success(userView), postDetailViewModel.user.value)
    }

    @Test
    fun setData() {

        //Add a mock observer to our LiveData so that we can verify the data is changed when we expect it to.
        val userIdObserver = Mockito.mock(Observer::class.java) as Observer<String>
        postDetailViewModel._userId.observeForever(userIdObserver)

        val postIdObserver = Mockito.mock(Observer::class.java) as Observer<String>
        postDetailViewModel._postId.observeForever(postIdObserver)

        //Test Function
        postDetailViewModel.setData(1, 2)

        Mockito.verify(userIdObserver).onChanged("1")
        Assert.assertEquals("1", postDetailViewModel._userId.value)

        Mockito.verify(postIdObserver).onChanged("2")
        Assert.assertEquals("2", postDetailViewModel._postId.value)

    }
}
package com.babylon.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.babylon.presentation.model.PostView
import com.babylon.presentation.repository.PostDataRepository
import com.babylon.presentation.resource.Resource
import org.junit.*
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext
import org.koin.standalone.StandAloneContext.startKoin
import org.koin.standalone.inject
import org.koin.test.KoinTest
import org.mockito.Mockito

class PostListViewModelTest : KoinTest {

    // Executes tasks in the Architecture Components in the same thread
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val postListViewModel : PostListViewModel by inject()

    private val postDataRepository = Mockito.mock(PostDataRepository::class.java)

    @Before
    fun setUp() {
        startKoin(listOf(
            module {
                viewModel {
                    PostListViewModel(
                        postDataRepository = postDataRepository
                    )
                }
            }
        ))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun getData() {
        //Add a mock observer to our LiveData so that we can verify the data is changed when we expect it to.
        val observer = Mockito.mock(Observer::class.java) as Observer<Resource<List<PostView>>>
        postListViewModel.postList.observeForever(observer)

        val result = MutableLiveData<Resource<List<PostView>>>()
        val postViewList = PostTestData.postViewList
        result.value = Resource.success(postViewList)
        Mockito.`when`(
            postDataRepository.getPosts()
        ).thenReturn(
            result
        )

        //Call the function manually as it depends on the Activity's lifecycle to trigger it
        postListViewModel.getData()

        Mockito.verify(observer).onChanged(Resource.success(postViewList))
        Assert.assertEquals(Resource.success(postViewList), postListViewModel.postList.value)
    }
}
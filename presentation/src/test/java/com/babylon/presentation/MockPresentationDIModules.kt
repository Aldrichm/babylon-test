package com.babylon.presentation

import com.babylon.presentation.viewmodel.PostDetailViewModel
import com.babylon.presentation.viewmodel.PostListViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val mockPresentationDIModule = module {

    viewModel {
        PostListViewModel(
            postDataRepository = get()
        )
    }

    viewModel {
        PostDetailViewModel(
            userDataRepository = get(),
            commentDataRepository = get(),
            postDataRepository = get()
        )
    }
}
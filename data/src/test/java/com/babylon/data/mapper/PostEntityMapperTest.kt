package com.babylon.data.mapper

import com.babylon.data.mockDataDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class PostEntityMapperTest : KoinTest {

    private val postEntityMapper: PostEntityMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockDataDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun mapToView() {
        val postEntity = PostTestData.postEntity
        val result = postEntityMapper.mapToView(postEntity)
        Assert.assertEquals(result.body, postEntity.body)
        Assert.assertEquals(result.title, postEntity.title)
        Assert.assertEquals(result.id, postEntity.id)
        Assert.assertEquals(result.userId, postEntity.userId)
    }
}
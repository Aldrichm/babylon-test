package com.babylon.data.mapper

import com.babylon.data.mockDataDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class UserEntityMapperTest : KoinTest {

    private val userEntityMapper: UserEntityMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockDataDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun mapToView() {
        val userEntity = UserTestData.userEntity
        val result = userEntityMapper.mapToView(userEntity)
        Assert.assertEquals(result.email, userEntity.email)
        Assert.assertEquals(result.id, userEntity.id)
        Assert.assertEquals(result.name, userEntity.name)
        Assert.assertEquals(result.phone, userEntity.phone)
        Assert.assertEquals(result.website, userEntity.website)
    }
}
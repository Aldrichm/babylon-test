package com.babylon.data.mapper

import com.babylon.data.mockDataDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class CommentEntityMapperTest : KoinTest {

    private val commentEntityMapper: CommentEntityMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockDataDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun mapToView() {
        val commentEntity = CommentTestData.commentEntity
        val result = commentEntityMapper.mapToView(commentEntity)
        Assert.assertEquals(result.id, commentEntity.id)
        Assert.assertEquals(result.body, commentEntity.body)
        Assert.assertEquals(result.email, commentEntity.email)
        Assert.assertEquals(result.name, commentEntity.name)
        Assert.assertEquals(result.postId, commentEntity.postId)
    }
}
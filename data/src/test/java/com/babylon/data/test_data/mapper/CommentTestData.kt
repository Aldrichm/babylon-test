import com.babylon.data.model.CommentEntity
import com.babylon.presentation.model.CommentView

object CommentTestData {

    val commentEntity = CommentEntity(
        postId = 1,
        id = 1,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val commentView = CommentView(
        postId = 1,
        id = 1,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )
}
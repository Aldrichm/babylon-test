import com.babylon.data.model.PostEntity
import com.babylon.presentation.model.PostView

object PostTestData {

    val postEntity = PostEntity(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val postView = PostView(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )
}
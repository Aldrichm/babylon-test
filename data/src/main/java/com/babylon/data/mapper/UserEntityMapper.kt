package com.babylon.data.mapper

import com.babylon.data.model.UserEntity
import com.babylon.presentation.model.UserView

/**
 * Map a [UserEntity] instance to a [UserView] instance when data is moving between
 * this later and the Data layer
 */
class UserEntityMapper() : DataMapper<UserView, UserEntity> {

    override fun mapToView(type: UserEntity): UserView {
        return UserView(
            id = type.id,
            email = type.email,
            name = type.name,
            phone = type.phone,
            username = type.username,
            website = type.website
        )    }
}
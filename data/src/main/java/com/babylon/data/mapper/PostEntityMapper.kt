package com.babylon.data.mapper

import com.babylon.data.model.PostEntity
import com.babylon.presentation.model.PostView


/**
 * Map a [PostEntity] instance to a [PostView] instance when data is moving between
 * this later and the Presentation layer
 */
class PostEntityMapper() : DataMapper<PostView, PostEntity> {

    override fun mapToView(type: PostEntity): PostView {
        return PostView(
            userId = type.userId,
            id = type.id,
            title = type.title,
            body = type.body
        )
    }
}
package com.babylon.data.mapper

import com.babylon.data.model.CommentEntity
import com.babylon.presentation.model.CommentView

/**
 * Map a [CommentEntity] instance to a [CommentView] instance when data is moving between
 * this later and the Presentation layer
 */
class CommentEntityMapper() : DataMapper<CommentView, CommentEntity> {

    override fun mapToView(type: CommentEntity): CommentView {
        return CommentView(
            postId = type.postId,
            id = type.id,
            body = type.body,
            name = type.name,
            email = type.email
        )    }
}
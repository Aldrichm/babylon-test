package com.babylon.data.model

data class UserEntity(
    val id: Int,
    val email: String,
    val name: String,
    val phone: String,
    val username: String,
    val website: String
)
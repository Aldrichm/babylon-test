package com.babylon.data.util.extensions

fun Any.tag(): String {
    return this::class.java.simpleName
}
package com.babylon.data.di

import com.babylon.data.mapper.CommentEntityMapper
import com.babylon.data.mapper.PostEntityMapper
import com.babylon.data.mapper.UserEntityMapper
import com.babylon.data.source.CommentDataRepositoryImpl
import com.babylon.data.source.PostDataRepositoryImpl
import com.babylon.data.source.UserDataRepositoryImpl
import com.babylon.presentation.repository.CommentDataRepository
import com.babylon.presentation.repository.PostDataRepository
import com.babylon.presentation.repository.UserDataRepository
import org.koin.dsl.module.module

val dataDIModule = module {

    single<CommentDataRepository> {
        CommentDataRepositoryImpl(
            remoteCommentRepository = get(),
            cachedCommentRepository = get(),
            commentEntityMapper = get(),
            schedulerProvider = get()
        )
    }
    single<PostDataRepository> {
        PostDataRepositoryImpl(
            schedulerProvider = get(),
            postEntityMapper = get(),
            cachedPostRepository = get(),
            remotePostRepository = get()
        )
    }

    single<UserDataRepository> {
        UserDataRepositoryImpl(
            schedulerProvider = get(),
            cachedUserRepository = get(),
            remoteUserRepository = get(),
            userEntityMapper = get()
        )
    }

    factory { UserEntityMapper() }
    factory { PostEntityMapper() }
    factory { CommentEntityMapper() }

}
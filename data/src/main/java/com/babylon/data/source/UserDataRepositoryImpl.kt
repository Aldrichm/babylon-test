package com.babylon.data.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.babylon.data.mapper.UserEntityMapper
import com.babylon.data.model.UserEntity
import com.babylon.data.repository.CachedUserRepository
import com.babylon.data.repository.RemoteUserRepository
import com.babylon.data.resource.NetworkBoundResource
import com.babylon.data.util.SchedulerProvider
import com.babylon.presentation.model.UserView
import com.babylon.presentation.repository.UserDataRepository
import com.babylon.presentation.resource.Resource
import io.reactivex.Completable
import io.reactivex.Single

class UserDataRepositoryImpl(
    private val schedulerProvider: SchedulerProvider,
    private val userEntityMapper: UserEntityMapper,
    private val cachedUserRepository: CachedUserRepository,
    private val remoteUserRepository: RemoteUserRepository
) : UserDataRepository {

    override fun getUserWithId(id: String): LiveData<Resource<UserView>> {

        return object : NetworkBoundResource<UserView, UserEntity>(schedulerProvider) {

            override fun processResponse(response: UserEntity): UserEntity {
                return response
            }

            override fun saveCallResult(item: UserEntity): Completable {
                return cachedUserRepository.saveUser(item)
            }

            override fun shouldFetch(data: UserView?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<UserView> {
                return Transformations.map(cachedUserRepository.getUserWithId(id)) {
                    val firstResult = it.firstOrNull()
                    firstResult?.let {
                        userEntityMapper.mapToView(it)
                    }
                }
            }

            override fun createCall(): Single<UserEntity> {
                return remoteUserRepository.getUserWithID(id)
            }
        }.asLiveData()
    }
}
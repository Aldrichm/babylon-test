package com.babylon.data.source

import android.net.Network
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.babylon.data.mapper.PostEntityMapper
import com.babylon.data.model.PostEntity
import com.babylon.data.repository.CachedPostRepository
import com.babylon.data.repository.RemotePostRepository
import com.babylon.data.resource.NetworkBoundResource
import com.babylon.data.util.SchedulerProvider
import com.babylon.presentation.model.PostView
import com.babylon.presentation.repository.PostDataRepository
import com.babylon.presentation.resource.Resource
import io.reactivex.Completable
import io.reactivex.Single

class PostDataRepositoryImpl(
    private val schedulerProvider: SchedulerProvider,
    private val postEntityMapper: PostEntityMapper,
    private val cachedPostRepository: CachedPostRepository,
    private val remotePostRepository: RemotePostRepository
) : PostDataRepository {

    override fun getPosts(): LiveData<Resource<List<PostView>>> {

        return object : NetworkBoundResource<List<PostView>, List<PostEntity>>(schedulerProvider) {


            override fun processResponse(response: List<PostEntity>): List<PostEntity> {
                return response
            }

            override fun saveCallResult(item: List<PostEntity>): Completable {
                return cachedPostRepository.savePosts(postList = item)
            }

            override fun shouldFetch(data: List<PostView>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<PostView>> {
                return Transformations.map(cachedPostRepository.getPosts()) {
                    val postViewList = mutableListOf<PostView>()
                    it.mapTo(postViewList) {
                        postEntityMapper.mapToView(it)
                    }
                    return@map postViewList
                }
            }

            override fun createCall(): Single<List<PostEntity>> {
                return remotePostRepository.getPosts()
            }
        }.asLiveData()

    }

    override fun getPostWithId(id: String): LiveData<Resource<PostView>> {

        return object : NetworkBoundResource<PostView, PostEntity>(schedulerProvider) {
            override fun processResponse(response: PostEntity): PostEntity {
                return response
            }

            override fun saveCallResult(item: PostEntity): Completable {
                return cachedPostRepository.savePosts(listOf(item))
            }

            override fun shouldFetch(data: PostView?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<PostView> {
                return Transformations.map(cachedPostRepository.getPostWithId(id)) {
                    val firstResult = it.firstOrNull()
                    firstResult?.let {
                        postEntityMapper.mapToView(it)
                    }
                }
            }

            override fun createCall(): Single<PostEntity> {
                return remotePostRepository.getPostWithId(id)
            }
        }.asLiveData()
    }
}
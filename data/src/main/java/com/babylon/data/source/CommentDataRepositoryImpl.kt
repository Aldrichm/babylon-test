package com.babylon.data.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.babylon.data.mapper.CommentEntityMapper
import com.babylon.data.model.CommentEntity
import com.babylon.data.repository.CachedCommentRepository
import com.babylon.data.repository.RemoteCommentRepository
import com.babylon.data.resource.NetworkBoundResource
import com.babylon.data.util.SchedulerProvider
import com.babylon.presentation.model.CommentView
import com.babylon.presentation.repository.CommentDataRepository
import com.babylon.presentation.resource.Resource
import io.reactivex.Completable
import io.reactivex.Single

class CommentDataRepositoryImpl(
    private val remoteCommentRepository: RemoteCommentRepository,
    private val cachedCommentRepository: CachedCommentRepository,
    private val commentEntityMapper: CommentEntityMapper,
    private val schedulerProvider: SchedulerProvider
) : CommentDataRepository {

    override fun getCommentsForPostId(postId: String): LiveData<Resource<List<CommentView>>> {

        return object : NetworkBoundResource<List<CommentView>, List<CommentEntity>>(schedulerProvider) {

            override fun processResponse(response: List<CommentEntity>): List<CommentEntity> {
                return response
            }

            override fun saveCallResult(item: List<CommentEntity>): Completable {
                return cachedCommentRepository.saveComments(commentList = item)
            }

            override fun shouldFetch(data: List<CommentView>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<CommentView>> {
                return Transformations.map(cachedCommentRepository.getCommentsForPostId(postId = postId)) {
                    val commentViewList = mutableListOf<CommentView>()
                    it.mapTo(commentViewList) {
                        commentEntityMapper.mapToView(it)
                    }
                    return@map commentViewList
                }
            }

            override fun createCall(): Single<List<CommentEntity>> {
                return remoteCommentRepository.getCommentsWithParams(postId = postId)
            }
        }.asLiveData()
    }
}
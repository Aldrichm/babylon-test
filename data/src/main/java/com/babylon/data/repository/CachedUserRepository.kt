package com.babylon.data.repository

import androidx.lifecycle.LiveData
import com.babylon.data.model.UserEntity
import io.reactivex.Completable

interface CachedUserRepository {

    fun getUserWithId(id: String): LiveData<List<UserEntity>>

    fun saveUser(userEntity: UserEntity): Completable

}
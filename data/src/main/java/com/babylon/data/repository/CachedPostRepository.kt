package com.babylon.data.repository

import androidx.lifecycle.LiveData
import com.babylon.data.model.PostEntity
import io.reactivex.Completable

interface CachedPostRepository {

    fun savePosts(postList: List<PostEntity>): Completable

    fun getPosts(): LiveData<List<PostEntity>>

    fun getPostWithId(id : String): LiveData<List<PostEntity>>

}
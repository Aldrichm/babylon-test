package com.babylon.data.repository

import androidx.lifecycle.LiveData
import com.babylon.data.model.CommentEntity
import io.reactivex.Completable

interface CachedCommentRepository {

    fun getCommentsForPostId(postId : String) : LiveData<List<CommentEntity>>

    fun saveComments(commentList: List<CommentEntity>): Completable
}
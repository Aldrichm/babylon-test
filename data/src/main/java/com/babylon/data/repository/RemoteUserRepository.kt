package com.babylon.data.repository

import com.babylon.data.model.UserEntity
import io.reactivex.Single

interface RemoteUserRepository {

    fun getUserWithID(userID: String): Single<UserEntity>

}
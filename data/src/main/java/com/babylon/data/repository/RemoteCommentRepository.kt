package com.babylon.data.repository

import com.babylon.data.model.CommentEntity
import io.reactivex.Single

interface RemoteCommentRepository {

    fun getCommentsWithParams(postId: String): Single<List<CommentEntity>>
}
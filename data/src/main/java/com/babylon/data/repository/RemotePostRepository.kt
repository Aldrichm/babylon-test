package com.babylon.data.repository

import com.babylon.data.model.PostEntity
import io.reactivex.Single

interface RemotePostRepository {

    fun getPosts(): Single<List<PostEntity>>

    fun getPostWithId(id: String): Single<PostEntity>
}
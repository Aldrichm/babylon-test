package com.babylon.remote.repository

import com.babylon.data.model.CommentEntity
import com.babylon.data.repository.RemoteCommentRepository
import com.babylon.remote.mockRemoteDIModule
import io.reactivex.observers.TestObserver
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class RemoteCommentRepositoryImplTest : KoinTest {

    private val remoteCommentRepository: RemoteCommentRepository by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockRemoteDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun getCommentsWithParams() {
        val source = remoteCommentRepository.getCommentsWithParams("1")

        val observer: TestObserver<List<CommentEntity>> = TestObserver.create()
        source.subscribe(observer)

        observer.assertNoErrors()
        observer.assertComplete()

        Assert.assertEquals(observer.valueCount(), 1)

    }
}
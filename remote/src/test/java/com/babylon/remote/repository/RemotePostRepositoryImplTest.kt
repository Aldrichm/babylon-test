package com.babylon.remote.repository

import com.babylon.data.model.PostEntity
import com.babylon.data.repository.RemotePostRepository
import com.babylon.remote.mockRemoteDIModule
import io.reactivex.observers.TestObserver
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class RemotePostRepositoryImplTest : KoinTest{

    private val remotePostRepository : RemotePostRepository by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockRemoteDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun getPosts() {
        val source = remotePostRepository.getPosts()

        val observer : TestObserver<List<PostEntity>> = TestObserver.create()
        source.subscribe(observer)

        observer.assertNoErrors()
        observer.assertComplete()

        Assert.assertEquals(observer.valueCount(), 1)

    }

    @Test
    fun getPostWithId() {
        val source = remotePostRepository.getPostWithId("1")

        val observer : TestObserver<PostEntity> = TestObserver.create()
        source.subscribe(observer)

        observer.assertNoErrors()
        observer.assertComplete()

        Assert.assertEquals(observer.valueCount(), 1)

    }
}
package com.babylon.remote

import com.babylon.data.repository.RemoteCommentRepository
import com.babylon.data.repository.RemotePostRepository
import com.babylon.data.repository.RemoteUserRepository
import com.babylon.remote.deserializer.CommentsResponseDeserializer
import com.babylon.remote.deserializer.PostsResponseDeserializer
import com.babylon.remote.deserializer.UserResponseDeserializer
import com.babylon.remote.mapper.RemoteCommentMapper
import com.babylon.remote.mapper.RemotePostMapper
import com.babylon.remote.mapper.RemoteUserMapper
import com.babylon.remote.repository.RemoteCommentRepositoryImpl
import com.babylon.remote.repository.RemotePostRepositoryImpl
import com.babylon.remote.repository.RemoteUserRepositoryImpl
import com.babylon.remote.response.CommentsResponse
import com.babylon.remote.response.PostsResponse
import com.babylon.remote.response.UserResponse
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module.module

/**
 * Koin Mock Module to provide dependenices for all Network Related Classes
 */
val mockRemoteDIModule = module {

    single { CommentsResponseDeserializer() }
    single { PostsResponseDeserializer() }
    single { UserResponseDeserializer() }

    single {
        createGson(
            commentsResponseDeserializer = get(),
            postsResponseDeserializer = get(),
            userResponseDeserializer = get()
        )
    }

    single<APIService> { MockAPIService() }

    //Repository
    factory<RemoteCommentRepository> {
        RemoteCommentRepositoryImpl(
            apiService = get(),
            remoteCommentMapper = get()
        )
    }

    factory<RemoteUserRepository> {
        RemoteUserRepositoryImpl(
            apiService = get(),
            remoteUserMapper = get()
        )
    }

    factory<RemotePostRepository> {
        RemotePostRepositoryImpl(
            apiService = get(),
            remotePostMapper = get()
        )
    }

    //Mappers
    factory { RemotePostMapper() }
    factory { RemoteUserMapper() }
    factory { RemoteCommentMapper() }
}

fun createGson(
    commentsResponseDeserializer: CommentsResponseDeserializer,
    postsResponseDeserializer: PostsResponseDeserializer,
    userResponseDeserializer: UserResponseDeserializer
): Gson {
    return GsonBuilder()
        .registerTypeAdapter(CommentsResponse::class.java, commentsResponseDeserializer)
        .registerTypeAdapter(PostsResponse::class.java, postsResponseDeserializer)
        .registerTypeAdapter(UserResponse::class.java, userResponseDeserializer)
        .create()
}

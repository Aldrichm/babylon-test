package com.babylon.remote

import com.babylon.remote.response.CommentsResponse
import com.babylon.remote.response.PostsResponse
import com.babylon.remote.response.UserResponse
import com.babylon.remote.test_data.network.CommentResponseTestData
import com.babylon.remote.test_data.network.PostsResponseTestData
import com.babylon.remote.test_data.network.UserResponseTestData
import io.reactivex.Single

class MockAPIService : APIService {

    override fun getPosts(): Single<PostsResponse> {
        val postsResponse = PostsResponseTestData.postsResponse
        return Single.just(postsResponse)
    }

    override fun getPostWithId(id: String): Single<PostsResponse> {
        val postsResponse = PostsResponseTestData.postsResponse
        return Single.just(postsResponse)
    }

    override fun getUserWithID(userID: String): Single<UserResponse> {
        val userResponse = UserResponseTestData.userResponse
        return Single.just(userResponse)
    }

    override fun getCommentsWithParams(postId: String): Single<CommentsResponse> {
        val commentsResponse = CommentResponseTestData.commentsResponse
        return Single.just(commentsResponse)
    }
}
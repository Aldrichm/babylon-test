package com.babylon.remote.test_data.network

import com.babylon.remote.response.PostsResponse
import com.babylon.remote.response.RemotePost

object PostsResponseTestData{

    val remotePost1 = RemotePost(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val remotePost2 = RemotePost(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val postsResponse = PostsResponse(
        remotePostList = listOf(remotePost1, remotePost2)
    )

}
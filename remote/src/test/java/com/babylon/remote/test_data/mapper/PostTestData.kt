import com.babylon.data.model.PostEntity
import com.babylon.remote.response.RemotePost

object PostTestData{

    val remotePost = RemotePost(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val postEntity = PostEntity(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )
}
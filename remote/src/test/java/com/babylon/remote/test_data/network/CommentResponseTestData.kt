package com.babylon.remote.test_data.network

import com.babylon.remote.response.CommentsResponse
import com.babylon.remote.response.RemoteComment

object CommentResponseTestData {

    val remoteComment1 = RemoteComment(
        postId = 1,
        id = 1,
        name = "Testy TestFace1",
        email = "testy1@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val remoteComment2 = RemoteComment(
        postId = 2,
        id = 2,
        name = "Testy TestFace2",
        email = "testy2@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val commentsResponse = CommentsResponse(
        remoteCommentList = listOf(
            remoteComment1,
            remoteComment2
        )
    )

}
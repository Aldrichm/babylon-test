package com.babylon.remote.test_data.network

import com.babylon.remote.response.RemoteUser
import com.babylon.remote.response.UserResponse

object UserResponseTestData{

    val remoteUser = RemoteUser(
        id = 1,
        email = "testy@test.com",
        name = "Testy Mc Test face",
        phone = "0000-000-0000",
        username = "Testy",
        website = "testy@email.com"
    )

    val userResponse = UserResponse(
        remoteUserList = listOf(remoteUser)
    )

}
import com.babylon.data.model.CommentEntity
import com.babylon.remote.response.RemoteComment

object CommentTestData {

    val remoteComment = RemoteComment(
        postId = 1,
        id = 1,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val commentEntity = CommentEntity(
        postId = 1,
        id = 1,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )
}
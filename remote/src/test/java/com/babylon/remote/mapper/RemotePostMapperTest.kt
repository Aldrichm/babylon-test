package com.babylon.remote.mapper

import com.babylon.remote.mockRemoteDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class RemotePostMapperTest : KoinTest {

    private val remotePostMapper: RemotePostMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockRemoteDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun mapFromRemote() {
        val remotePost = PostTestData.remotePost
        val result = remotePostMapper.mapFromRemote(remotePost)
        Assert.assertEquals(result.body, remotePost.body)
        Assert.assertEquals(result.title, remotePost.title)
        Assert.assertEquals(result.id, remotePost.id)
        Assert.assertEquals(result.userId, remotePost.userId)
    }
}
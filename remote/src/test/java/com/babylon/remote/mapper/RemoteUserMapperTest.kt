package com.babylon.remote.mapper

import com.babylon.remote.mockRemoteDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class RemoteUserMapperTest : KoinTest {

    private val remoteUserMapper: RemoteUserMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockRemoteDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun mapFromRemote() {
        val remoteUser = UserTestData.remoteUser
        val result = remoteUserMapper.mapFromRemote(remoteUser)
        Assert.assertEquals(result.email, remoteUser.email)
        Assert.assertEquals(result.id, remoteUser.id)
        Assert.assertEquals(result.name, remoteUser.name)
        Assert.assertEquals(result.phone, remoteUser.phone)
        Assert.assertEquals(result.website, remoteUser.website)
    }
}
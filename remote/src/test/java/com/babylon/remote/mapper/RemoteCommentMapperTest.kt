package com.babylon.remote.mapper

import com.babylon.remote.mockRemoteDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class RemoteCommentMapperTest : KoinTest {

    private val remoteCommentMapper: RemoteCommentMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockRemoteDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun mapFromRemote() {
        val remoteComment = CommentTestData.remoteComment
        val result = remoteCommentMapper.mapFromRemote(remoteComment)
        Assert.assertEquals(result.id, remoteComment.id)
        Assert.assertEquals(result.body, remoteComment.body)
        Assert.assertEquals(result.email, remoteComment.email)
        Assert.assertEquals(result.name, remoteComment.name)
        Assert.assertEquals(result.postId, remoteComment.postId)
    }
}
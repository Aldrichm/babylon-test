package com.babylon.remote.deserializer

import com.babylon.remote.mockRemoteDIModule
import com.babylon.remote.response.PostsResponse
import com.google.gson.Gson
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest
import java.io.BufferedReader
import java.io.InputStreamReader

class PostsResponseDeserializerTest : KoinTest {


    private val gson: Gson by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockRemoteDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun deserialize() {
        //Load JSON from file
        val path = "PostsResponse.json"
        val target = InputStreamReader(this.javaClass.classLoader.getResourceAsStream(path))
        val bufferedReader = BufferedReader(target)

        val postsResponse = gson.fromJson(bufferedReader, PostsResponse::class.java)
        Assert.assertEquals(postsResponse.remotePostList.size, 100)

        val firstPost = postsResponse.remotePostList.first()
        Assert.assertEquals(
            firstPost.body,
            "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
        )
        Assert.assertEquals(firstPost.id, 1)
        Assert.assertEquals(
            firstPost.title,
            "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
        )
        Assert.assertEquals(firstPost.userId, 1)

        val secondPostWithEmptyValues = postsResponse.remotePostList[1]
        Assert.assertEquals(secondPostWithEmptyValues.userId, 0)
        Assert.assertEquals(secondPostWithEmptyValues.title, "")

    }
}
package com.babylon.remote.deserializer

import com.babylon.remote.mockRemoteDIModule
import com.babylon.remote.response.CommentsResponse
import com.google.gson.Gson
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest
import java.io.BufferedReader
import java.io.InputStreamReader

class CommentsResponseDeserializerTest : KoinTest {

    private val gson: Gson by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockRemoteDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun deserialize() {

        //Load JSON from file
        val path = "CommentsResponse.json"
        val target = InputStreamReader(this.javaClass.classLoader.getResourceAsStream(path))
        val bufferedReader = BufferedReader(target)

        val commentsResponse = gson.fromJson(bufferedReader, CommentsResponse::class.java)
        Assert.assertEquals(commentsResponse.remoteCommentList.size, 5)

        val firstComment = commentsResponse.remoteCommentList.first()
        Assert.assertEquals(firstComment.body, "laudantium enim quasi est quidem magnam voluptate ipsam eos\ntempora quo necessitatibus\ndolor quam autem quasi\nreiciendis et nam sapiente accusantium")
        Assert.assertEquals(firstComment.email, "Eliseo@gardner.biz")
        Assert.assertEquals(firstComment.id, 1)
        Assert.assertEquals(firstComment.name, "id labore ex et quam laborum")
        Assert.assertEquals(firstComment.postId, 1)

    }
}
package com.babylon.remote.deserializer

import com.babylon.remote.mockRemoteDIModule
import com.babylon.remote.response.UserResponse
import com.google.gson.Gson
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest
import java.io.BufferedReader
import java.io.InputStreamReader

class UserResponseDeserializerTest : KoinTest {

    private val gson: Gson by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockRemoteDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun deserialize() {

        //Load JSON from file
        val path = "UserResponse.json"
        val target = InputStreamReader(this.javaClass.classLoader.getResourceAsStream(path))
        val bufferedReader = BufferedReader(target)

        val userResponse = gson.fromJson(bufferedReader, UserResponse::class.java)
        Assert.assertEquals(userResponse.remoteUserList.size, 1)

        val firstUser = userResponse.remoteUserList.first()

        Assert.assertEquals(firstUser.email, "Sincere@april.biz")
        Assert.assertEquals(firstUser.name, "Leanne Graham")
        Assert.assertEquals(firstUser.phone, "1-770-736-8031 x56442")
        Assert.assertEquals(firstUser.id, 1)
        Assert.assertEquals(firstUser.username, "Bret")
        Assert.assertEquals(firstUser.website, "hildegard.org")

    }
}
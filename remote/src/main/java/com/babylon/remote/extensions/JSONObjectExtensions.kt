package com.babylon.remote.extensions

import com.google.gson.JsonObject

fun <T> JsonObject.defensiveTry(default: T, func: (JsonObject) -> T): T {
    return try {
        func(this)
    } catch (exception: Exception) {
        default
    }
}
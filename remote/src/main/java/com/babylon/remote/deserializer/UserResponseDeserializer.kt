package com.babylon.remote.deserializer

import com.babylon.remote.extensions.defensiveTry
import com.babylon.remote.response.RemoteUser
import com.babylon.remote.response.UserResponse
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.lang.reflect.Type

class UserResponseDeserializer : JsonDeserializer<UserResponse> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): UserResponse {

        var userResponse = UserResponse(remoteUserList = emptyList())

        json?.asJsonArray?.let {

            val userList = ArrayList<RemoteUser>()

            it.mapTo(userList) {
                with(it as JsonObject) {

                    val id = defensiveTry(0) { get("id").asInt }
                    val email = defensiveTry("") { get("email").asString }
                    val name = defensiveTry("") { get("name").asString }
                    val phone = defensiveTry("") { get("phone").asString }
                    val username = defensiveTry("") { get("username").asString }
                    val website = defensiveTry("") { get("website").asString }

                    return@mapTo RemoteUser(
                        id = id,
                        email = email,
                        name = name,
                        phone = phone,
                        username = username,
                        website = website
                    )
                }
            }
            userResponse = UserResponse(remoteUserList = userList)
        }
        return userResponse
    }
}
package com.babylon.remote.deserializer

import com.babylon.remote.extensions.defensiveTry
import com.babylon.remote.response.RemotePost
import com.babylon.remote.response.PostsResponse
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.lang.reflect.Type

class PostsResponseDeserializer : JsonDeserializer<PostsResponse> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): PostsResponse {

        var postsResponse = PostsResponse(remotePostList = emptyList())

        json?.asJsonArray?.let {

            val postList = ArrayList<RemotePost>()

            it.mapTo(postList) {
                with(it as JsonObject) {

                    val userId = defensiveTry(0) { get("userId").asInt }
                    val id = defensiveTry(0) { get("id").asInt }
                    val title = defensiveTry("") { get("title").asString }
                    val body = defensiveTry("") { get("body").asString }

                    return@mapTo RemotePost(
                        userId = userId,
                        id = id,
                        title = title,
                        body = body
                    )
                }
            }
            postsResponse = PostsResponse(remotePostList = postList)
        }
        return postsResponse
    }
}
package com.babylon.remote.deserializer

import com.babylon.remote.extensions.defensiveTry
import com.babylon.remote.response.RemoteComment
import com.babylon.remote.response.CommentsResponse
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import java.lang.reflect.Type

class CommentsResponseDeserializer : JsonDeserializer<CommentsResponse> {

    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): CommentsResponse {

        var commentsResponse = CommentsResponse(remoteCommentList = emptyList())

        json?.asJsonArray?.let {

            val commentList = ArrayList<RemoteComment>()

            it.mapTo(commentList) {
                with(it as JsonObject) {

                    val postId = defensiveTry(0) { get("postId").asInt }
                    val id = defensiveTry(0) { get("id").asInt }
                    val name = defensiveTry("") { get("name").asString }
                    val email = defensiveTry("") { get("email").asString }
                    val body = defensiveTry("") { get("body").asString }

                    return@mapTo RemoteComment(
                        postId = postId,
                        id = id,
                        name = name,
                        email = email,
                        body = body
                    )
                }
            }
            commentsResponse = CommentsResponse(remoteCommentList = commentList)
        }
        return commentsResponse
    }
}
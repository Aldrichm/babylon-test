package com.babylon.remote.di

import com.babylon.data.repository.RemoteCommentRepository
import com.babylon.data.repository.RemotePostRepository
import com.babylon.data.repository.RemoteUserRepository
import com.babylon.remote.APIService
import com.babylon.remote.deserializer.CommentsResponseDeserializer
import com.babylon.remote.deserializer.PostsResponseDeserializer
import com.babylon.remote.deserializer.UserResponseDeserializer
import com.babylon.remote.mapper.RemoteCommentMapper
import com.babylon.remote.mapper.RemotePostMapper
import com.babylon.remote.mapper.RemoteUserMapper
import com.babylon.remote.repository.RemoteCommentRepositoryImpl
import com.babylon.remote.repository.RemotePostRepositoryImpl
import com.babylon.remote.repository.RemoteUserRepositoryImpl
import com.babylon.remote.response.CommentsResponse
import com.babylon.remote.response.PostsResponse
import com.babylon.remote.response.UserResponse
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Koin Module to provide dependenices for all Network Related Classes
 */
val remoteDIModule = module {

    single { CommentsResponseDeserializer() }
    single { PostsResponseDeserializer() }
    single { UserResponseDeserializer() }

    single {
        createGson(
            commentsResponseDeserializer = get(),
            postsResponseDeserializer = get(),
            userResponseDeserializer = get()
        )
    }

    single { createOkHttpClient() }

    single {
        createRetrofitService<APIService>(
            okHttpClient = get(),
            url = getProperty("JSON_PLACEHOLDER_BASE_URL"),
            gson = get()
        )
    }

    //Repository
    factory<RemoteCommentRepository> {
        RemoteCommentRepositoryImpl(
            apiService = get(),
            remoteCommentMapper = get()
        )
    }

    factory<RemoteUserRepository> {
        RemoteUserRepositoryImpl(
            apiService = get(),
            remoteUserMapper = get()
        )
    }

    factory<RemotePostRepository> {
        RemotePostRepositoryImpl(
            apiService = get(),
            remotePostMapper = get()
        )
    }

    //Mappers
    factory { RemotePostMapper() }
    factory { RemoteUserMapper() }
    factory { RemoteCommentMapper() }
}

fun createOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    return OkHttpClient.Builder()
        .connectTimeout(60L, TimeUnit.SECONDS)
        .readTimeout(60L, TimeUnit.SECONDS)
        .addInterceptor(httpLoggingInterceptor)
        .build()
}

inline fun <reified T> createRetrofitService(okHttpClient: OkHttpClient, url: String, gson: Gson): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}

fun createGson(
    commentsResponseDeserializer: CommentsResponseDeserializer,
    postsResponseDeserializer: PostsResponseDeserializer,
    userResponseDeserializer: UserResponseDeserializer
): Gson {
    return GsonBuilder()
        .registerTypeAdapter(CommentsResponse::class.java, commentsResponseDeserializer)
        .registerTypeAdapter(PostsResponse::class.java, postsResponseDeserializer)
        .registerTypeAdapter(UserResponse::class.java, userResponseDeserializer)
        .create()
}

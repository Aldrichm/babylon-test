package com.babylon.remote.response

data class PostsResponse(
    val remotePostList: List<RemotePost>
)

data class RemotePost(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String
)

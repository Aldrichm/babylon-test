package com.babylon.remote.response

data class UserResponse(
    val remoteUserList: List<RemoteUser>
)

data class RemoteUser(
    val id: Int,
    val email: String,
    val name: String,
    val phone: String,
    val username: String,
    val website: String
)

package com.babylon.remote.response

data class CommentsResponse(
    val remoteCommentList: List<RemoteComment>
)

data class RemoteComment(
    val postId: Int,
    val id: Int,
    val name: String,
    val email: String,
    val body: String
)

package com.babylon.remote.repository

import com.babylon.data.model.PostEntity
import com.babylon.data.repository.RemotePostRepository
import com.babylon.remote.APIService
import com.babylon.remote.mapper.RemotePostMapper
import io.reactivex.Single

class RemotePostRepositoryImpl(
    private val apiService: APIService,
    private val remotePostMapper: RemotePostMapper
) : RemotePostRepository {

    override fun getPosts(): Single<List<PostEntity>> {
        return apiService.getPosts()
            .map { it.remotePostList}
            .map {
                val postEntityList = mutableListOf<PostEntity>()
                it.mapTo(postEntityList){remotePost ->
                    remotePostMapper.mapFromRemote(remotePost)
                }
            }
    }

    override fun getPostWithId(id: String): Single<PostEntity> {
        return apiService.getPostWithId(id)
            .map {it.remotePostList.first() }
            .map {
                remotePostMapper.mapFromRemote(it)
            }
    }
}
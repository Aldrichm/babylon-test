package com.babylon.remote.repository

import com.babylon.data.model.CommentEntity
import com.babylon.data.repository.RemoteCommentRepository
import com.babylon.remote.APIService
import com.babylon.remote.mapper.RemoteCommentMapper
import io.reactivex.Single

class RemoteCommentRepositoryImpl(
    private val apiService: APIService,
    private val remoteCommentMapper: RemoteCommentMapper
) : RemoteCommentRepository {

    override fun getCommentsWithParams(postId: String): Single<List<CommentEntity>> {
        return apiService.getCommentsWithParams(postId = postId)
            .map { it.remoteCommentList }
            .map {
                val commentEntityList = mutableListOf<CommentEntity>()
                it.mapTo(commentEntityList) { remoteComment ->
                    remoteCommentMapper.mapFromRemote(remoteComment)
                }
            }
    }
}
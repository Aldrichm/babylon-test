package com.babylon.remote.repository

import com.babylon.data.model.UserEntity
import com.babylon.data.repository.RemoteUserRepository
import com.babylon.remote.APIService
import com.babylon.remote.mapper.RemoteUserMapper
import io.reactivex.Single

class RemoteUserRepositoryImpl(
    private val apiService: APIService,
    private val remoteUserMapper: RemoteUserMapper
) : RemoteUserRepository {

    override fun getUserWithID(userID: String): Single<UserEntity> {
            return apiService.getUserWithID(userID = userID)
                .map { it.remoteUserList.first() }
                .map { remoteUser ->
                    remoteUserMapper.mapFromRemote(remoteUser)
                }
    }

}
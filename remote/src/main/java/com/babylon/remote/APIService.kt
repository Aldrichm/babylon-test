package com.babylon.remote

import com.babylon.remote.response.CommentsResponse
import com.babylon.remote.response.PostsResponse
import com.babylon.remote.response.UserResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIService {

    @GET("posts/")
    fun getPosts() : Single<PostsResponse>

    @GET("posts")
    fun getPostWithId(@Query("id") id : String) : Single<PostsResponse>

    @GET("users")
    fun getUserWithID(@Query("id") userID : String) : Single<UserResponse>

    @GET("comments")
    fun getCommentsWithParams(@Query("postId") postId : String) : Single<CommentsResponse>

}
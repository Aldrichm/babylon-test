package com.babylon.remote.mapper

/**
 * Mapping Remote Models to the out layer model
 *
 * @param <M> the remote model input type
 * @param <E> the entity model output type
 */
interface RemoteMapper<in M, out E> {

    fun mapFromRemote(type: M): E

}
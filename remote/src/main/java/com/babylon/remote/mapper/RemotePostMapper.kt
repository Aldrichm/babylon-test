package com.babylon.remote.mapper

import com.babylon.data.model.PostEntity
import com.babylon.remote.response.RemotePost


/**
 * Map a [RemotePost] instance to a [PostEntity] instance when data is moving between
 * this later and the Data layer
 */
class RemotePostMapper() : RemoteMapper<RemotePost, PostEntity> {

    override fun mapFromRemote(type: RemotePost): PostEntity {
        return PostEntity(
            userId = type.userId,
            id = type.id,
            title = type.title,
            body = type.body
        )
    }
}
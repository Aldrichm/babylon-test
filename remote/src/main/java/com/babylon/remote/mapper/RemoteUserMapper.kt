package com.babylon.remote.mapper

import com.babylon.data.model.UserEntity
import com.babylon.remote.response.RemoteUser

/**
 * Map a [RemoteUser] instance to a [UserEntity] instance when data is moving between
 * this later and the Data layer
 */
class RemoteUserMapper() : RemoteMapper<RemoteUser, UserEntity> {

    override fun mapFromRemote(type: RemoteUser): UserEntity {
        return UserEntity(
            id = type.id,
            email = type.email,
            name = type.name,
            phone = type.phone,
            username = type.username,
            website = type.website
        )
    }

}
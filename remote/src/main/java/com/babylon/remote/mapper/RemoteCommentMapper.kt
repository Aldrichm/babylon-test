package com.babylon.remote.mapper

import com.babylon.data.model.CommentEntity
import com.babylon.remote.response.RemoteComment

/**
 * Map a [RemoteComment] instance to a [CommentEntity] instance when data is moving between
 * this later and the Data layer
 */
class RemoteCommentMapper() : RemoteMapper<RemoteComment, CommentEntity> {

    override fun mapFromRemote(type: RemoteComment): CommentEntity {
        return CommentEntity(
            postId = type.postId,
            id = type.id,
            body = type.body,
            name = type.name,
            email = type.email
        )
    }

}
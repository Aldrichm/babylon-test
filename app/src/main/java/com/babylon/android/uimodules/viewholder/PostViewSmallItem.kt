package com.babylon.android.uimodules.viewholder

import android.view.View
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.babylon.android.R
import com.babylon.android.uimodules.helper.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_post_layout_small)
abstract class PostViewSmallItem : EpoxyModelWithHolder<PostViewSmallHolder>() {

    @EpoxyAttribute
    lateinit var title: String
    @EpoxyAttribute
    lateinit var body: String
    @EpoxyAttribute
    lateinit var author: String
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var clickListener: View.OnClickListener

    override fun bind(holder: PostViewSmallHolder) {
        with(holder){
            titleTextView.text = title
            authorTextView.text = "by $author"
            bodyTextView.text = body
            cardView.setOnClickListener(clickListener)
        }
    }
}

class PostViewSmallHolder : KotlinEpoxyHolder(){
    val cardView by bind<CardView>(R.id.itemPostLayoutSmallCardView)
    val titleTextView by bind<TextView>(R.id.itemPostLayoutSmallTextViewPostTitle)
    val authorTextView by bind<TextView>(R.id.itemPostLayoutSmallTextViewAuthor)
    val bodyTextView by bind<TextView>(R.id.itemPostLayoutSmallTextViewBody)
}
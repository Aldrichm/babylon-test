package com.babylon.android.uimodules.viewholder

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.babylon.android.R
import com.babylon.android.uimodules.helper.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_user_layout)
abstract class UserViewItem : EpoxyModelWithHolder<UserViewHolder>() {


    @EpoxyAttribute
    lateinit var username: String
    @EpoxyAttribute
    lateinit var name: String

    override fun bind(holder: UserViewHolder) {
        with(holder){
            usernameTextView.text = username
            nameTextView.text = name
        }
    }
}


class UserViewHolder : KotlinEpoxyHolder(){
    val usernameTextView by bind<TextView>(R.id.itemUserLayoutTextViewUsername)
    val nameTextView by bind<TextView>(R.id.itemUserLayoutTextViewName)
}
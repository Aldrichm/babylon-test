package com.babylon.android.uimodules.viewholder

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.babylon.android.R
import com.babylon.android.uimodules.helper.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_post_layout_detailed)
abstract class PostViewDetailedItem : EpoxyModelWithHolder<PostViewDetailedHolder>() {

    @EpoxyAttribute
    lateinit var title: String
    @EpoxyAttribute
    lateinit var body: String

    override fun bind(holder: PostViewDetailedHolder) {
        with(holder) {
            titleTextView.text = title
            bodyTextView.text = body
        }
    }
}


class PostViewDetailedHolder : KotlinEpoxyHolder() {
    val titleTextView by bind<TextView>(R.id.itemPostLayoutDetailedTextViewPostTitle)
    val bodyTextView by bind<TextView>(R.id.itemPostLayoutDetailedTextViewBody)
}
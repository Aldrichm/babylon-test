package com.babylon.android.uimodules.viewholder

import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.babylon.android.R
import com.babylon.android.uimodules.helper.KotlinEpoxyHolder

@EpoxyModelClass(layout = R.layout.item_comment_layout)
abstract class CommentViewItem : EpoxyModelWithHolder<CommentViewHolder>() {

    @EpoxyAttribute
    lateinit var email: String
    @EpoxyAttribute
    lateinit var name: String
    @EpoxyAttribute
    lateinit var body: String

    override fun bind(holder: CommentViewHolder) {
        with(holder) {
            emailTextView.text = email
            nameTextView.text = name
            bodyTextView.text = body
        }
    }
}


class CommentViewHolder : KotlinEpoxyHolder() {
    val emailTextView by bind<TextView>(R.id.itemCommentLayoutTextViewEmail)
    val nameTextView by bind<TextView>(R.id.itemCommentLayoutTextViewName)
    val bodyTextView by bind<TextView>(R.id.itemCommentLayoutTextViewBody)
}
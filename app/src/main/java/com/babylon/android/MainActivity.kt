package com.babylon.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController

class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Set Toolbar
        setupToolBar()

        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.main_activity_nav_host_fragment) as NavHostFragment? ?: return

        // Get Navigation Controller
        val navController = host.navController
        //Initialise AppBarConfiguration
        appBarConfiguration = AppBarConfiguration.Builder(setOf(R.id.postListFragment)).build()

        setupActionBar(navController, appBarConfiguration)

    }

    private fun setupToolBar() {
        val toolbar = findViewById<Toolbar>(R.id.main_activity_toolbar)
        setSupportActionBar(toolbar)
    }

    private fun setupActionBar(
        navController: NavController,
        appBarConfig: AppBarConfiguration
    ) {
        // This allows NavigationUI to decide what label to show in the action bar
        // By using appBarConfig, it will also determine whether to
        // show the up arrow or drawer menu icon
        setupActionBarWithNavController(navController, appBarConfig)
    }

    override fun onSupportNavigateUp(): Boolean {// Up button will work on this method
        return Navigation.findNavController(this, R.id.main_activity_nav_host_fragment).navigateUp()
    }

}

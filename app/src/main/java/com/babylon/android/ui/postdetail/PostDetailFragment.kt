package com.babylon.android.ui.postdetail


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.babylon.android.R
import com.babylon.presentation.model.CommentView
import com.babylon.presentation.model.PostView
import com.babylon.presentation.model.UserView
import com.babylon.presentation.resource.Resource
import com.babylon.presentation.viewmodel.PostDetailViewModel
import kotlinx.android.synthetic.main.fragment_post_detail.*
import org.koin.android.viewmodel.ext.android.viewModel

class PostDetailFragment : Fragment() {

    private val viewModel: PostDetailViewModel by viewModel()
    private val postDetailAdapterController: PostDetailAdapterController by lazy { PostDetailAdapterController() }

    //Args
    private var userId: Int = 0
    private var postId: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userId = arguments?.getInt("userId", 0) ?: 0
        postId = arguments?.getInt("postId", 0) ?: 0

        viewModel.setData(userId = userId, postId = postId)

        //Set LifecycleObserver
        lifecycle.addObserver(viewModel)

        viewModel.post.observe(this,
            Observer {
                handlePostState(it)
            })

        viewModel.user.observe(this,
            Observer {
                handleUserState(it)
            })


        viewModel.commentList.observe(this,
            Observer {
                handleCommentListState(it)
            })

        //Setup RecyclerView
        initRecyclerView()

    }

    private fun initRecyclerView() {
        fragmentPostDetailRecyclerView.layoutManager = LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
        fragmentPostDetailRecyclerView.adapter = postDetailAdapterController.adapter
    }


    private fun handlePostState(resource: Resource<PostView>) {
        when (resource) {
            is Resource.Loading -> {
            }
            is Resource.Success -> postDetailAdapterController.setContent(postView = resource.data)
            is Resource.Error -> {
                resource.data?.let {
                    postDetailAdapterController.setContent(postView = it)
                }
            }
        }
    }


    private fun handleCommentListState(resource: Resource<List<CommentView>>) {
        when (resource) {
            is Resource.Loading -> {
            }
            is Resource.Success -> postDetailAdapterController.setContent(commentViewList = resource.data)
            is Resource.Error -> {
                resource.data?.let {
                    postDetailAdapterController.setContent(commentViewList = it)
                }
            }
        }
    }

    private fun handleUserState(resource: Resource<UserView>) {
        when (resource) {
            is Resource.Loading -> {
                fragmentPostDetailProgress.visibility = View.VISIBLE
            }
            is Resource.Success -> {
                postDetailAdapterController.setContent(userView = resource.data)
                fragmentPostDetailProgress.visibility = View.GONE
            }
            is Resource.Error -> {
                resource.data?.let {
                    postDetailAdapterController.setContent(userView = it)
                }
                fragmentPostDetailProgress.visibility = View.GONE
            }
        }
    }

}

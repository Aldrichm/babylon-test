package com.babylon.android.ui.postlist


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.babylon.android.R
import com.babylon.presentation.model.PostView
import com.babylon.presentation.resource.Resource
import com.babylon.presentation.viewmodel.PostListViewModel
import kotlinx.android.synthetic.main.fragment_post_list.*
import org.koin.android.viewmodel.ext.android.viewModel


class PostListFragment : Fragment() {

    private val viewModel: PostListViewModel by viewModel()
    private val postListAdapterController = PostListAdapterController { onPostViewItemClick(it) }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_post_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Set LifecycleObserver
        lifecycle.addObserver(viewModel)

        //Observe State in ViewModel
        viewModel.postList.observe(this,
            Observer {
                handlePostListState(it)
            })

        //Setup RecyclerView
        initRecyclerView()

    }

    private fun initRecyclerView() {
        fragmentPostListRecyclerView.layoutManager = LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)
        fragmentPostListRecyclerView.adapter = postListAdapterController.adapter
    }

    private fun handlePostListState(resource: Resource<List<PostView>>) {
        when (resource) {
            is Resource.Loading -> {
                fragmentPostListProgress.visibility = View.VISIBLE
            }
            is Resource.Success -> {
                fragmentPostListProgress.visibility = View.GONE
                postListAdapterController.setContent(resource.data)
            }
            is Resource.Error -> {
                fragmentPostListProgress.visibility = View.GONE
                resource.data?.let {
                    postListAdapterController.setContent(it)
                }
            }
        }
    }

    private fun onPostViewItemClick(postView: PostView) {
        val action = PostListFragmentDirections.actionPostListFragmentToPostDetailFragment(
            userId = postView.userId,
            postId = postView.id
        )
        Navigation.findNavController(view!!).navigate(action)
    }
}

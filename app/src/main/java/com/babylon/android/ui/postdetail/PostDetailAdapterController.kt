package com.babylon.android.ui.postdetail

import com.airbnb.epoxy.EpoxyController
import com.babylon.android.uimodules.viewholder.commentViewItem
import com.babylon.android.uimodules.viewholder.postViewDetailedItem
import com.babylon.android.uimodules.viewholder.userViewItem
import com.babylon.presentation.model.CommentView
import com.babylon.presentation.model.PostView
import com.babylon.presentation.model.UserView
import java.util.*

class PostDetailAdapterController : EpoxyController() {

    private var userView: UserView? = null
    private var detailedPost: PostView? = null
    private var commentViewList: List<CommentView>? = listOf()

    fun setContent(postView: PostView) {
        this.detailedPost = postView
        requestModelBuild()
    }

    fun setContent(commentViewList: List<CommentView>) {
        this.commentViewList = commentViewList
        requestModelBuild()
    }

    fun setContent(userView: UserView) {
        this.userView = userView
        requestModelBuild()
    }
    override fun buildModels() {

        detailedPost?.let {
            postViewDetailedItem {
                id(it.hashCode())
                title(it.title)
                body(it.body)
            }
        }

        userView?.let {
            userViewItem {
                id(it.hashCode())
                username(it.username)
                name(it.name)
            }
        }

        commentViewList?.let {
            it.forEach {
                commentViewItem {
                    id(it.hashCode())
                    email(it.email)
                    name(it.name)
                    body(it.body)
                }
            }
        }


    }


}
package com.babylon.android.ui.postlist

import com.airbnb.epoxy.EpoxyController
import com.babylon.android.uimodules.viewholder.postViewSmallItem
import com.babylon.presentation.model.PostView

class PostListAdapterController(private val itemClickCallback: (PostView) -> Unit) : EpoxyController() {

    private var postViewList= listOf<PostView>()

    fun setContent(db: List<PostView>) {
        postViewList = db
        requestModelBuild()
    }

    override fun buildModels() {

        postViewList.forEach {
            postViewSmallItem {
                id(it.id)
                title(it.title)
                author("Default")
                body(it.body)
                clickListener { _, _, _, position ->
                    itemClickCallback(postViewList[position])
                }
            }
        }
    }


}
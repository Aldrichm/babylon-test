package com.babylon.android

import android.app.Application
import com.babylon.di.*
import org.koin.android.ext.android.startKoin
import org.koin.standalone.StandAloneContext

class MainApplication() : Application() {

    override fun onCreate() {
        super.onCreate()

        StandAloneContext
        startKoin(
            androidContext = this,
            modules = listOf(coreModule, cacheDIModule, remoteDIModule, dataDIModule, presentationDIModule),
            loadPropertiesFromFile = true
        )

    }
}

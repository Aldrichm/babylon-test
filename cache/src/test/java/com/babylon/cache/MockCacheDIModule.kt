package com.babylon.cache

import android.content.Context
import org.koin.dsl.module.module
import androidx.room.Room
import com.babylon.cache.mapper.CachedCommentMapper
import com.babylon.cache.mapper.CachedPostMapper
import com.babylon.cache.mapper.CachedUserMapper
import com.babylon.cache.repository.CachedCommentRepositoryImpl
import com.babylon.cache.repository.CachedPostRepositoryImpl
import com.babylon.cache.repository.CachedUserRepositoryImpl
import com.babylon.data.repository.CachedCommentRepository
import com.babylon.data.repository.CachedPostRepository
import com.babylon.data.repository.CachedUserRepository
import org.koin.android.ext.koin.androidApplication

/**
 * Mock Koin Module to provide dependenices for all Database Related Classes
 */
val mockCacheDIModule = module {

    single { createMoneseDatabase(androidApplication()) }

    factory<CachedCommentRepository> {
        CachedCommentRepositoryImpl(
            dbService = get(),
            cachedCommentMapper = get()
        )
    }

    factory<CachedPostRepository> {
        CachedPostRepositoryImpl(
            dbService = get(),
            cachedPostMapper = get()
        )
    }

    factory<CachedUserRepository> {
        CachedUserRepositoryImpl(
            dbService = get(),
            cachedUserMapper = get()
        )
    }

    factory { CachedUserMapper() }
    factory { CachedPostMapper() }
    factory { CachedCommentMapper() }
}

//TODO : Mock this to create a Room DB in memory
fun createMoneseDatabase(context: Context): DBService =
    Room.databaseBuilder(context, DBService::class.java, "babylon_db").build()


import com.babylon.cache.model.CachedPost
import com.babylon.data.model.PostEntity

object PostTestData {

    val cachedPost = CachedPost(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val postEntity = PostEntity(
        userId = 1,
        id = 1,
        title = "lorem ipsum doler",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )
}
import com.babylon.cache.model.CachedComment
import com.babylon.data.model.CommentEntity

object CommentTestData {

    val cachedComment = CachedComment(
        postId = 1,
        id = 1,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )

    val commentEntity = CommentEntity(
        postId = 1,
        id = 1,
        name = "Testy TestFace",
        email = "testy@test.com",
        body = "lorem ipsum doler lorem ipsum doler lorem ipsum doler"
    )
}
import com.babylon.cache.model.CachedUser
import com.babylon.data.model.UserEntity


object UserTestData{

    val cachedUser = CachedUser(
        id = 1,
        email = "testy@test.com",
        name = "Testy Mc Test face",
        phone = "0000-000-0000",
        username = "Testy",
        website = "testy@email.com"
    )

    val userEntity = UserEntity(
        id = 1,
        email = "testy@test.com",
        name = "Testy Mc Test face",
        phone = "0000-000-0000",
        username = "Testy",
        website = "testy@email.com"
    )

}
package com.babylon.cache.mapper

import com.babylon.cache.mockCacheDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class CachedUserMapperTest : KoinTest {

    private val cachedUserMapper: CachedUserMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockCacheDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun mapFromCached() {
        val cachedUser = UserTestData.cachedUser
        val result = cachedUserMapper.mapFromCached(cachedUser)
        Assert.assertEquals(result.email, cachedUser.email)
        Assert.assertEquals(result.id, cachedUser.id)
        Assert.assertEquals(result.name, cachedUser.name)
        Assert.assertEquals(result.phone, cachedUser.phone)
        Assert.assertEquals(result.website, cachedUser.website)
    }

    @Test
    fun mapToCached() {
        val userEntity = UserTestData.userEntity
        val result = cachedUserMapper.mapToCached(userEntity)
        Assert.assertEquals(result.email, userEntity.email)
        Assert.assertEquals(result.id, userEntity.id)
        Assert.assertEquals(result.name, userEntity.name)
        Assert.assertEquals(result.phone, userEntity.phone)
        Assert.assertEquals(result.website, userEntity.website)
    }
}
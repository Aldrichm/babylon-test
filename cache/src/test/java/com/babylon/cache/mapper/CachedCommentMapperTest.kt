package com.babylon.cache.mapper

import com.babylon.cache.mockCacheDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class CachedCommentMapperTest : KoinTest {

    private val cachedCommentMapper : CachedCommentMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockCacheDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }


    @Test
    fun mapFromCached() {
        val cachedComment = CommentTestData.cachedComment
        val result = cachedCommentMapper.mapFromCached(cachedComment)
        Assert.assertEquals(result.id, cachedComment.id)
        Assert.assertEquals(result.body, cachedComment.body)
        Assert.assertEquals(result.email, cachedComment.email)
        Assert.assertEquals(result.name, cachedComment.name)
        Assert.assertEquals(result.postId, cachedComment.postId)
    }

    @Test
    fun mapToCached() {
        val commentEntity = CommentTestData.commentEntity
        val result = cachedCommentMapper.mapToCached(commentEntity)
        Assert.assertEquals(result.id, commentEntity.id)
        Assert.assertEquals(result.body, commentEntity.body)
        Assert.assertEquals(result.email, commentEntity.email)
        Assert.assertEquals(result.name, commentEntity.name)
        Assert.assertEquals(result.postId, commentEntity.postId)
    }
}

package com.babylon.cache.mapper

import com.babylon.cache.mockCacheDIModule
import org.junit.After
import org.junit.Assert
import org.junit.Test
import org.junit.Before
import org.koin.standalone.StandAloneContext
import org.koin.standalone.inject
import org.koin.test.KoinTest

class CachedPostMapperTest : KoinTest {

    private val cachedPostMapper: CachedPostMapper by inject()

    @Before
    fun setUp() {
        StandAloneContext.startKoin(listOf(mockCacheDIModule))
    }

    @After
    fun tearDown() {
        StandAloneContext.stopKoin()
    }

    @Test
    fun mapFromCached() {
        val cachedPost = PostTestData.cachedPost
        val result = cachedPostMapper.mapFromCached(cachedPost)
        Assert.assertEquals(result.body, cachedPost.body)
        Assert.assertEquals(result.title, cachedPost.title)
        Assert.assertEquals(result.id, cachedPost.id)
        Assert.assertEquals(result.userId, cachedPost.userId)
    }

    @Test
    fun mapToCached() {
        val postEntity = PostTestData.postEntity
        val result = cachedPostMapper.mapToCached(postEntity)
        Assert.assertEquals(result.body, postEntity.body)
        Assert.assertEquals(result.title, postEntity.title)
        Assert.assertEquals(result.id, postEntity.id)
        Assert.assertEquals(result.userId, postEntity.userId)
    }
}
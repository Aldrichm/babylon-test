package com.babylon.cache.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.babylon.cache.model.CachedComment
import io.reactivex.Completable

@Dao
interface CachedCommentDao{

    @Query("SELECT * from COMMENT where  postId = :postId ")
    fun getCommentsForPostId(postId : String) : LiveData<List<CachedComment>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveComments(commentList: List<CachedComment>): Completable

}
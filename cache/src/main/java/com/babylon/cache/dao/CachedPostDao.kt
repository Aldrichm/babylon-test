package com.babylon.cache.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.babylon.cache.model.CachedPost
import io.reactivex.Completable

@Dao
interface CachedPostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun savePosts(postList: List<CachedPost>): Completable

    @Query("SELECT * FROM POST")
    fun getPosts(): LiveData<List<CachedPost>>

    @Query("SELECT * FROM POST WHERE id = :id")
    fun getPostWithId(id : String): LiveData<List<CachedPost>>

}
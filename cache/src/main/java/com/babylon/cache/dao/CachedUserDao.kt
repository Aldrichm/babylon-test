package com.babylon.cache.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.babylon.cache.model.CachedUser
import io.reactivex.Completable

@Dao
interface CachedUserDao {

    @Query("SELECT * FROM USER WHERE id = :id")
    fun getUserWithId(id: String): LiveData<List<CachedUser>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveUser(cachedUser: CachedUser): Completable

}
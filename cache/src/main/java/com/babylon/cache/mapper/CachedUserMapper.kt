package com.babylon.cache.mapper

import com.babylon.cache.model.CachedUser
import com.babylon.data.model.UserEntity

/**
 * Map a [CachedUser] instance to and from a [UserEntity] instance when data is moving between
 * this layer and the Data layer
 */
class CachedUserMapper() : CachedMapper<CachedUser, UserEntity> {


    override fun mapFromCached(type: CachedUser): UserEntity {
        return  UserEntity(
            id = type.id,
            email = type.email,
            name = type.name,
            phone = type.phone,
            username = type.username,
            website = type.website
        )
    }

    override fun mapToCached(type: UserEntity): CachedUser {
        return  CachedUser(
            id = type.id,
            email = type.email,
            name = type.name,
            phone = type.phone,
            username = type.username,
            website = type.website
        )
    }
}
package com.babylon.cache.mapper

import com.babylon.cache.model.CachedPost
import com.babylon.data.model.PostEntity


/**
 * Map a [CachedPost] instance to and from a [PostEntity] instance when data is moving between
 * this later and the Data layer
 */
class CachedPostMapper() : CachedMapper<CachedPost, PostEntity> {

    override fun mapFromCached(type: CachedPost): PostEntity {
        return PostEntity(
            userId = type.userId,
            id = type.id,
            title = type.title,
            body = type.body
        )
    }

    override fun mapToCached(type: PostEntity): CachedPost {
        return CachedPost(
            userId = type.userId,
            id = type.id,
            title = type.title,
            body = type.body
        )
    }
}
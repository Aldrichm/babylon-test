package com.babylon.cache.mapper

import com.babylon.cache.model.CachedComment
import com.babylon.data.model.CommentEntity

/**
 * Map a [CachedComment] instance to and from a [CommentEntity] instance when data is moving between
 * this later and the Data layer
 */
class CachedCommentMapper() : CachedMapper<CachedComment, CommentEntity> {

    override fun mapFromCached(type: CachedComment): CommentEntity {
        return CommentEntity(
            postId = type.postId,
            id = type.id,
            body = type.body,
            name = type.name,
            email = type.email
        )
    }

    override fun mapToCached(type: CommentEntity): CachedComment {
        return CachedComment(
            postId = type.postId,
            id = type.id,
            body = type.body,
            name = type.name,
            email = type.email
        )
    }
}
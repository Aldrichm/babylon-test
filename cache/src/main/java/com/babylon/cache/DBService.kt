package com.babylon.cache

import androidx.room.Database
import androidx.room.RoomDatabase
import com.babylon.cache.dao.CachedCommentDao
import com.babylon.cache.dao.CachedPostDao
import com.babylon.cache.dao.CachedUserDao
import com.babylon.cache.model.CachedComment
import com.babylon.cache.model.CachedPost
import com.babylon.cache.model.CachedUser


@Database(
    entities = [
        CachedComment::class,
        CachedPost::class,
        CachedUser::class
    ],
    version = 3
)
abstract class DBService : RoomDatabase() {

    abstract fun cachedCommentDao(): CachedCommentDao

    abstract fun cachedUserDao(): CachedUserDao

    abstract fun cachedPostDao(): CachedPostDao

}
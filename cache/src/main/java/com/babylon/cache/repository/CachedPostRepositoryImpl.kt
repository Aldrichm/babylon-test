package com.babylon.cache.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.babylon.cache.DBService
import com.babylon.cache.mapper.CachedPostMapper
import com.babylon.cache.model.CachedPost
import com.babylon.data.model.PostEntity
import com.babylon.data.repository.CachedPostRepository
import io.reactivex.Completable

class CachedPostRepositoryImpl(
    private val dbService: DBService,
    private val cachedPostMapper: CachedPostMapper
) : CachedPostRepository {

    override fun savePosts(postList: List<PostEntity>): Completable {
        val cachedPostList = mutableListOf<CachedPost>()
        postList.mapTo(cachedPostList) {
            cachedPostMapper.mapToCached(it)
        }
        return dbService.cachedPostDao().savePosts(postList = cachedPostList)
    }

    override fun getPosts(): LiveData<List<PostEntity>> {
        return Transformations.map(dbService.cachedPostDao().getPosts()) {

            val postEntityList = mutableListOf<PostEntity>()
            it.mapTo(postEntityList) {
                cachedPostMapper.mapFromCached(it)
            }
            return@map postEntityList
        }

    }

    override fun getPostWithId(id: String): LiveData<List<PostEntity>> {
        return Transformations.map(dbService.cachedPostDao().getPostWithId(id)) {
            val postEntityList = mutableListOf<PostEntity>()
            it.mapTo(postEntityList){
                cachedPostMapper.mapFromCached(it)
            }
            return@map postEntityList
        }
    }

}
package com.babylon.cache.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.babylon.cache.DBService
import com.babylon.cache.mapper.CachedCommentMapper
import com.babylon.cache.model.CachedComment
import com.babylon.data.model.CommentEntity
import com.babylon.data.repository.CachedCommentRepository
import io.reactivex.Completable

class CachedCommentRepositoryImpl(
    private val dbService: DBService,
    private val cachedCommentMapper: CachedCommentMapper
) : CachedCommentRepository {

    override fun getCommentsForPostId(postId: String): LiveData<List<CommentEntity>> {
        return Transformations.map(dbService.cachedCommentDao().getCommentsForPostId(postId)) {
            val commentEntityList = mutableListOf<CommentEntity>()
            it.mapTo(commentEntityList) {
                cachedCommentMapper.mapFromCached(it)
            }
            return@map commentEntityList
        }
    }

    override fun saveComments(commentList: List<CommentEntity>): Completable {
        val cachedCommentList = mutableListOf<CachedComment>()
        commentList.mapTo(cachedCommentList){
            cachedCommentMapper.mapToCached(it)
        }
        return dbService.cachedCommentDao().saveComments(commentList = cachedCommentList)
    }
}
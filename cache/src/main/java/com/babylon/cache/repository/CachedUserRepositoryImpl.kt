package com.babylon.cache.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.babylon.cache.DBService
import com.babylon.cache.mapper.CachedUserMapper
import com.babylon.data.model.UserEntity
import com.babylon.data.repository.CachedUserRepository
import io.reactivex.Completable

class CachedUserRepositoryImpl(
    private val dbService: DBService,
    private val cachedUserMapper: CachedUserMapper
) : CachedUserRepository {

    override fun getUserWithId(id: String): LiveData<List<UserEntity>> {
        return Transformations.map(dbService.cachedUserDao().getUserWithId(id)) {
            val userEntityList = mutableListOf<UserEntity>()
            it.mapTo(userEntityList){
                cachedUserMapper.mapFromCached(it)
            }
            return@map userEntityList
        }
    }

    override fun saveUser(userEntity: UserEntity): Completable {
        val cachedUser = cachedUserMapper.mapToCached(userEntity)
        return dbService.cachedUserDao().saveUser(cachedUser = cachedUser)
    }

}
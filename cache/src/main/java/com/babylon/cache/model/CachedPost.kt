package com.babylon.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "post")
data class CachedPost(
    val userId: Int,
    @PrimaryKey val id: Int,
    val title: String,
    val body: String
)

package com.babylon.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class CachedUser(
    @PrimaryKey val id: Int,
    val email: String,
    val name: String,
    val phone: String,
    val username: String,
    val website: String
)

package com.babylon.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "comment")
data class CachedComment(
    val postId: Int,
    @PrimaryKey val id: Int,
    val name: String,
    val email: String,
    val body: String
)
